#include "AbstractOpCode.h"

AbstractOpCode::AbstractOpCode(const char *name, const int code) : OP_CODE_NAME(name), OP_CODE_VALUE(code) { }

AbstractOpCode::~AbstractOpCode() { }

const char *AbstractOpCode::getName() {
    return this->OP_CODE_NAME;
}
const int AbstractOpCode::getCode() {
    return this->OP_CODE_VALUE;
}

