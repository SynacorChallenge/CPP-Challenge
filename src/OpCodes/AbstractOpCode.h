#ifndef SYNACORCHALLENGE_ABSTRACTOPCODE_H
#define SYNACORCHALLENGE_ABSTRACTOPCODE_H


class AbstractOpCode {
public:
    AbstractOpCode(const char *name, const int code);
    virtual ~AbstractOpCode();
    const char *getName();
    const int getCode();
    virtual void execute() = 0;
private:
    const char *OP_CODE_NAME;
    const int OP_CODE_VALUE;
};


#endif //SYNACORCHALLENGE_ABSTRACTOPCODE_H
