#include <iostream>
#include "HaltOpCode.h"

using namespace std;

HaltOpCode::HaltOpCode() : AbstractOpCode("HALT", 0) {
}
HaltOpCode::~HaltOpCode() {

}
void HaltOpCode::execute() {
    cout << "HALTING!" << endl;
}

