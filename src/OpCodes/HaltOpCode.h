#ifndef SYNACORCHALLENGE_HALTOPCODE_H
#define SYNACORCHALLENGE_HALTOPCODE_H

#include "AbstractOpCode.h"

class HaltOpCode : public AbstractOpCode {
public:
    HaltOpCode();
    virtual ~HaltOpCode();
    virtual void execute();
};


#endif //SYNACORCHALLENGE_HALTOPCODE_H
