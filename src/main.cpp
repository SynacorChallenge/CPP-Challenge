#include <iostream>
#include "OpCodes/HaltOpCode.h"

using namespace std;

int main() {
    HaltOpCode hoc;
    cout << "OpCode " << hoc.getCode() << ":  " << hoc.getName() << endl;
    return 0;
}